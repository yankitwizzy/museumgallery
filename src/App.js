import React, {useState} from 'react';
import {Layout, Card, Row, Col, Spin, Empty} from 'antd';
import './App.css';
import 'antd/dist/antd.css';
import {debounce} from "lodash";
import ObjectDetails from './components/ObjectDetails';
import ObjectSearchBox from './components/ObjectSearchBox';

export default function App() {

    const [results, setResults] = useState({});
    const [selectedResult, setSelectedResult] = useState({});
    const [search, setSearch] = useState("");
    const [isSearching, setIsSearching] = useState(false);
    const [visible, setVisible] = useState(false);

    /**
     * show the art work details
     * @param objectId
     */
    const showDetails = (objectId) => {
        setSelectedResult(results[objectId]);
        setVisible(true);
    };

    /**
     * called when details flyout is closed
     */
    const onDetailsClose = () => {
        setSelectedResult({});
        setVisible(false);
    };

    /**
     * fetch results of search
     * @param query
     * @returns {Promise<void>}
     */
    const fetchResults = async (query) => {
        setIsSearching(true);
        try {
            const url = `https://collectionapi.metmuseum.org/public/collection/v1/search?q=${query}`;
            const response = await fetch(url, {
                method: 'GET'
            });
            const json = await response.json();
            if (json && json.objectIDs) {
                const objectIds = json.objectIDs;
                const twentyIds = objectIds.slice(0, 20);
                const objects = {};
                let index = 0;
                twentyIds.forEach(async (id) => {
                    const objectUrl = `https://collectionapi.metmuseum.org/public/collection/v1/objects/${id}`;
                    const oResponse = await fetch(objectUrl, {
                        method: 'GET'
                    });
                    const oJson = await oResponse.json();
                    objects[id] = oJson;
                    index++;
                    if (index === twentyIds.length) {
                        setResults(objects);
                        setIsSearching(false);
                    }
                });
            } else {
                setIsSearching(false);
                return;
            }
        } catch (e) {
            alert("Error fetching museum data");
            setIsSearching(false);
        }
    }

    /**
     * handle searching for art work
     * @type {function(...[*]=)}
     */
    const handleSearch = debounce((e) => {
        const query = e.target.value;
        setResults({});
        setSearch(query);
        if (query) {
            fetchResults(query.toLowerCase());
        }
    }, 1200, false);

    /**
     * display search results
     * @returns {[]}
     */
    const renderResults = () => {
        if (Object.values(results).length === 0) {
            return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
        }
        const works = [];
        Object.values(results).forEach((object, index) => {
            works.push(<Col key={`col-${index}`} span={6}>
                <Card onClick={() => showDetails(object.objectID)}
                      hoverable
                      className="card"
                      cover={<img alt="artwork" src={object.primaryImageSmall}/>}
                >
                    <Card.Meta title={object.title} description={object.objectURL}/>
                </Card>
            </Col>)
        })
        return works;
    }

    return (
        <div>
            <Layout>
                <Layout.Header className="header">
                    <ObjectSearchBox isSearching={isSearching} handleSearch={handleSearch}/>
                </Layout.Header>
                {!isSearching ? <Layout.Content className="content">
                        <Row gutter={16}>
                            {renderResults()}
                        </Row>
                    </Layout.Content>
                    : <Layout.Content className="content"><Spin className="spin" tip="Searching..." size="large"/></Layout.Content>}
            </Layout>
            <ObjectDetails obj={selectedResult} onClose={onDetailsClose} visible={visible}/>
        </div>
    );
}
