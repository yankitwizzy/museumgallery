import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';

describe("App Test", () => {
  beforeAll(() => {
    Object.defineProperty(window, "matchMedia", {
      value: jest.fn(() => {
        return {
          matches: true,
          addListener: jest.fn(),
          removeListener: jest.fn()
        };
      })
    });
  });

  it("renders the app", () => {
    const component = render(<App />);
    expect(component).toBeTruthy();
  });
});
