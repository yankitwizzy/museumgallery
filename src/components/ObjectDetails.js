import React from 'react';
import {Drawer, Descriptions} from 'antd';

/**
 * component to display museum object details
 * @param obj
 * @param onClose
 * @param visible
 * @returns {*}
 * @constructor
 */
export default function ObjectDetails({obj, onClose, visible}) {
    return (
        <div>
            <Drawer
                className="drawer"
                title="Art Details"
                width="80%"
                closable={false}
                onClose={onClose}
                visible={visible}
            >
                <div className="details">
                    {
                        obj && (<div>
                            <div align="center"><img alt="art" src={obj.primaryImage} width={500}/><br/></div>
                            <Descriptions layout="vertical" bordered>
                                <Descriptions.Item span={3} label="Title">{obj.title}</Descriptions.Item>
                                <Descriptions.Item span={3} label="Department">{obj.department}</Descriptions.Item>
                                <Descriptions.Item span={3} label="Culture">{obj.culture}</Descriptions.Item>
                                <Descriptions.Item span={3} label="Dimensions">{obj.dimensions}</Descriptions.Item>
                                <Descriptions.Item span={3} label="Credit Line">{obj.creditLine}</Descriptions.Item>
                                <Descriptions.Item span={3} label="Repository">{obj.repository}</Descriptions.Item>
                                <Descriptions.Item span={3} label="URL">
                                    <a href={obj.objectURL} target="_blank"
                                       rel="noopener noreferrer">{obj.objectURL}</a>
                                </Descriptions.Item>
                            </Descriptions></div>)
                    }
                </div>
            </Drawer>
        </div>
    );
}
