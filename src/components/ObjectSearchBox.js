import React from 'react';
import {Input} from 'antd';

/**
 * component to search for museum objects
 * @param isSearching
 * @param handleSearch
 * @returns {*}
 * @constructor
 */
export default function ObjectSearchBox({isSearching, handleSearch}) {
    return (
        <Input
            size="large"
            disabled={isSearching}
            placeholder="Enter your search text here to search the Metropolitan Museum of Art collection"
            className="search"
            onChange={(e) => {
                handleSearch.cancel(); // cancel whatever already exists
                e.persist()
                handleSearch(e);
            }}
            />
    );
}
