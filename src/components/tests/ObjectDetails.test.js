import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import ObjectDetails from "../ObjectDetails";

describe("Object Details Test", () => {

    const obj = {
        "objectID": 254218,
        "accessionNumber": "41.162.51",
        "accessionYear": "1941",
        "primaryImage": "https://images.metmuseum.org/CRDImages/gr/original/DP114701.jpg",
        "primaryImageSmall": "https://images.metmuseum.org/CRDImages/gr/web-large/DP114701.jpg",
        "department": "Greek and Roman Art",
        "objectName": "Oinochoe, fragmentary",
        "title": "Fragment of a terracotta oinochoe (jug)",
        "culture": "East Greek, Wild Goat Style",
        "period": "Archaic",
        "objectDate": "7th century B.C.",
        "objectBeginDate": -699,
        "objectEndDate": -600,
        "medium": "Terracotta",
        "dimensions": "H. 2 15/16 in. (7.5 cm)",
        "creditLine": "Rogers Fund, 1941",
        "geographyType": "",
        "repository": "Metropolitan Museum of Art, New York, NY",
        "objectURL": "https://www.metmuseum.org/art/collection/search/254218",
    };

    const onClose = jest.fn();

    beforeAll(() => {
        Object.defineProperty(window, "matchMedia", {
            value: jest.fn(() => {
                return {
                    matches: true,
                    addListener: jest.fn(),
                    removeListener: jest.fn()
                };
            })
        });
    });

    it("renders object details", async () => {
        const component = render(<ObjectDetails obj={obj} onClose={onClose} visible={true} />);
        const getByText = component.getByText;
        expect(component).toBeTruthy();
        await waitForElement(() => getByText(`${obj.title}`));
        await waitForElement(() => getByText(`${obj.department}`));
        await waitForElement(() => getByText(`${obj.culture}`));
        await waitForElement(() => getByText(`${obj.dimensions}`));
        await waitForElement(() => getByText(`${obj.creditLine}`));
        await waitForElement(() => getByText(`${obj.repository}`))
    });

    it("doesn't render object details when not visible", () => {
        const component = render(<ObjectDetails obj={obj} onClose={onClose} visible={false} />);
        const queryByText = component.queryByText;
        expect(component).toBeTruthy();
        const title = queryByText(`${obj.title}`);
        expect(title).toBeNull();
    });
});
