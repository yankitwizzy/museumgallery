import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import ObjectSearchBox from "../ObjectSearchBox";

describe("Object Details Test", () => {

    const handleSearch = jest.fn();
    handleSearch.cancel = jest.fn();

    beforeAll(() => {
        Object.defineProperty(window, "matchMedia", {
            value: jest.fn(() => {
                return {
                    matches: true,
                    addListener: jest.fn(),
                    removeListener: jest.fn()
                };
            })
        });
    });

    it("renders object search", async () => {
        const component = render(<ObjectSearchBox isSearching={false} handleSearch={handleSearch} />);
        expect(component).toBeTruthy();
        await waitForElement(() =>  component.getByPlaceholderText("Enter your search text here to search the Metropolitan Museum of Art collection"));
    });

    it("calls handleSearch when changed", () => {
        const component = render(<ObjectSearchBox isSearching={false} handleSearch={handleSearch} />);
        expect(component).toBeTruthy();
        const input = component.getByPlaceholderText("Enter your search text here to search the Metropolitan Museum of Art collection");
        fireEvent.change(input, { target: { value: "name" }});
        expect(handleSearch).toBeCalledTimes(1);
    });
});
